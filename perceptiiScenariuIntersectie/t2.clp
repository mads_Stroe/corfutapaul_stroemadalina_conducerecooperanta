; camion1 vrea sa vireze stanga
; pe banda din stanga vine o coloana de masini
; in coloana avem agentul care are mai mult de 3 masini in spatele lui asa ca lasa camionul sa vireze

; EVENIMENT
(ag_percept
	(percept_pobj ev1)
	(percept_pname isa)
	(percept_pval eveniment)
)

; DRUM
(ag_percept
	(percept_pobj drum1)
	(percept_pname isa)
	(percept_pval drum)
)

(ag_percept
	(percept_pobj drum1)
	(percept_pname partof)
	(percept_pval eveniment)
)

(ag_percept
	(percept_pobj drum2)
	(percept_pname isa)
	(percept_pval drum)
)

(ag_percept
	(percept_pobj drum2)
	(percept_pname partof)
	(percept_pval eveniment)
)

; MASINI

(ag_percept
	(percept_pobj camion1)
	(percept_pname isa)
	(percept_pval car)
)

(ag_percept
	(percept_pobj camion1)
	(percept_pname ison)
	(percept_pval drum1)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname isa)
	(percept_pval car)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname ison)
	(percept_pval drum1)
)

(ag_percept
	(percept_pobj masina3)
	(percept_pname isa)
	(percept_pval car)
)

(ag_percept
	(percept_pobj masina3)
	(percept_pname ison)
	(percept_pval drum1)
)

(ag_percept
	(percept_pobj masina4)
	(percept_pname isa)
	(percept_pval car)
)

(ag_percept
	(percept_pobj masina4)
	(percept_pname ison)
	(percept_pval drum1)
)

(ag_percept
	(percept_pobj masina5)
	(percept_pname isa)
	(percept_pval car)
)

(ag_percept
	(percept_pobj masina5)
	(percept_pname ison)
	(percept_pval drum1)
)

(ag_percept
	(percept_pobj masina6)
	(percept_pname isa)
	(percept_pval car)
)

(ag_percept
	(percept_pobj masina6)
	(percept_pname ison)
	(percept_pval drum1)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname inFrontOf)
	(percept_pval masina3)
)

(ag_percept
	(percept_pobj masina3)
	(percept_pname inFrontOf)
	(percept_pval masina4)
)

(ag_percept
	(percept_pobj masina4)
	(percept_pname inFrontOf)
	(percept_pval masina5)
)

(ag_percept
	(percept_pobj masina5)
	(percept_pname inFrontOf)
	(percept_pval masina6)
)
;;;;;;;; DIRECTII ;;;;;;;;;;

(ag_percept
	(percept_pobj masina2)
	(percept_pname direction)
	(percept_pval ahead)
)

(ag_percept
	(percept_pobj masina3)
	(percept_pname direction)
	(percept_pval ahead)
)

(ag_percept
	(percept_pobj masina4)
	(percept_pname direction)
	(percept_pval ahead)
)

(ag_percept
	(percept_pobj masina5)
	(percept_pname direction)
	(percept_pval ahead)
)

(ag_percept
	(percept_pobj masina6)
	(percept_pname direction)
	(percept_pval ahead)
)

(ag_percept
	(percept_pobj camion1)
	(percept_pname direction)
	(percept_pval left)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname nbOfCarsBehind)
	(percept_pval 4)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname nbOfCarsAhead)
	(percept_pval 0)
)