;--------------------------------------
;
;     Initializam manevrele
;
;--------------------------------------

(defrule AGENT::initCycle-left-overtaking
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-overtaking-left prohibited by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-left-maneuver) (bel_pval prohibited))) 
    ;(facts AGENT)
)

(defrule AGENT::initCycle-right-overtaking
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-overtaking-right prohibited by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-right-maneuver) (bel_pval prohibited))) 
    ;(facts AGENT)
)

(defrule AGENT::initCycle-move
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-move move by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-right-maneuver) (bel_pval move))) 
    ;(facts AGENT)
)

(defrule AGENT::initCycle-move-car
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-move-car by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname move-car) (bel_pval prohibited))) 
    ;(facts AGENT)
)

(defrule AGENT::initCycle-increase
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-increase by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname increase-velo) (bel_pval prohibited))) 
    ;(facts AGENT)
)

(defrule AGENT::initCycle-decrease
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-decrease by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname decrease-velo) (bel_pval prohibited))) 
    ;(facts AGENT)
)

(defrule AGENT::initCycle-parkEmptyPlace
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-parkEmptyPlace by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname park-empty-place) (bel_pval prohibited))) 
    ;(facts AGENT)
)

(defrule AGENT::initCycle-overtakingWhenBus
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-overtakingWhenBus by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-when-bus) (bel_pval prohibited))) 
    ;(facts AGENT)
)

(defrule AGENT::initCycle-waitForCarToPass
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-waitForCarToPass by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname wait-for-car) (bel_pval prohibited))) 
    ;(facts AGENT)
)

(defrule AGENT::initCycle-letCarMoveLeft
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-letCarMoveLeft by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname let-car-move-left) (bel_pval prohibited))) 
    ;(facts AGENT)
)


;;----------------------------------------------
;;
;;    Scenariu depasire Tramvai(stanga/dreapta)
;;
;;----------------------------------------------

;-----------------------------------------------
;
;     Reguli auxiliare
;
;-----------------------------------------------

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; SCENARIU INTERSECTIE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule AGENT::i1
    (timp (valoare ?t))

    (ag_bel (bel_type moment) (bel_pobj ?ps5) (bel_pname direction) (bel_pval ahead)) ;m2
    (ag_bel (bel_type moment) (bel_pobj ?ps6) (bel_pname direction) (bel_pval left)) ;c1

    (ag_bel (bel_type moment) (bel_pobj ?ps7) (bel_pname nbOfCarsBehind) (bel_pval ?carsBehind))
    (test (>= ?carsBehind 3))   

    (ag_bel (bel_type moment) (bel_pobj ?ps7) (bel_pname nbOfCarsAhead) (bel_pval ?carsAhead))
    (test (= ?carsAhead 0))    

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Coloana e formata din cel putin 3 masini in spatele agentului, deci las masina sa treaca. " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname allow-car-move-left) (bel_pval yes)))

    ;(facts AGENT)
)

(defrule AGENT::i2
    (timp (valoare ?t))

    (ag_bel (bel_type moment) (bel_pobj ?ps5) (bel_pname direction) (bel_pval ahead)) ;m2

    (ag_bel (bel_type moment) (bel_pobj ?ps7) (bel_pname nbOfCarsBehind) (bel_pval ?carsBehind))
    (test (>= ?carsBehind 3))   

    (ag_bel (bel_type moment) (bel_pobj ?ps7) (bel_pname nbOfCarsAhead) (bel_pval ?carsAhead))
    (test (= ?carsAhead 0))    

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Agentul poate continua drumul, camionul a virat stanga. " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes)))

    ;(facts AGENT)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; DEPASIRE SCENARIU CU BUS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule AGENT::b1
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps3) (bel_pname inFrontOf) (bel_pval masina1))
    (ag_bel (bel_type moment) (bel_pobj ?ps4) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps5) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps6) (bel_pname ison) (bel_pval locBus))
    (not(ag_bel (bel_type moment) (bel_pobj ?ps7) (bel_pname ison) (bel_pval banda1)))
    (not(ag_bel (bel_type moment) (bel_pobj ?ps8) (bel_pname isEmpty) (bel_pval notEmpty)))
    (not(ag_bel (bel_type moment) (bel_pobj ?ps9) (bel_pname signals) (bel_pval busSignal)))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Autobuzul este parcat, deci pot efectua depasirea. " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname overtake) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname free-lane) (bel_pval yes)))

    ;(facts AGENT)
)

(defrule AGENT::b2
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps3) (bel_pname inFrontOf) (bel_pval masina1))
    (ag_bel (bel_type moment) (bel_pobj ?ps4) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps5) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps6) (bel_pname distanceFromCarOnLane2) (bel_pval adequate))
    (ag_bel (bel_type moment) (bel_pobj ?ps7) (bel_pname visibility) (bel_pval yes))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Autobuzul nu este parcat, deci pot efectua depasirea pentru ca ma aflu departe si am vizibilitate. " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname overtake) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname free-lane) (bel_pval yes)))

    ;(facts AGENT)
)

(defrule AGENT::b3
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps3) (bel_pname inFrontOf) (bel_pval masina1))
    (ag_bel (bel_type moment) (bel_pobj ?ps4) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps5) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps6) (bel_pname ison) (bel_pval locBus))
    (ag_bel (bel_type moment) (bel_pobj ?ps6) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps7) (bel_pname isEmpty) (bel_pval empty))
    (ag_bel (bel_type moment) (bel_pobj ?ps8) (bel_pname signals) (bel_pval busSignal))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Autobuzul este parcat, semnalizeaza iesirea, in spatele busului banda este libera, pot efectua depasirea. " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname overtake) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname free-lane) (bel_pval no)))
    ;(facts AGENT)
)

(defrule AGENT::b4
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps3) (bel_pname inFrontOf) (bel_pval masina1))
    (ag_bel (bel_type moment) (bel_pobj ?ps4) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps5) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps6) (bel_pname ison) (bel_pval locBus))
    (ag_bel (bel_type moment) (bel_pobj ?ps7) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps8) (bel_pname isEmpty) (bel_pval notEmpty))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Autobuzul este parcat, un camion vine in spatele lui, astept sa treaca. " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname free-lane) (bel_pval no)))
    ;(facts AGENT)
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TRECERE PIETONI ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule AGENT::m1
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname viteza) (bel_pval 80))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval drum1))
    (not (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname ison) (bel_pval trecerePietoni)))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Vad trecere de pietoni goala, deci pot sa trec " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname isPedestrian) (bel_pval no)))
    (assert (ag_bel (bel_type moment) (bel_pname isSlowed) (bel_pval no)))
    (assert (ag_bel (bel_type moment) (bel_pname isIncreased) (bel_pval yes)))

    ;(facts AGENT)
)

(defrule AGENT::m2
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname viteza) (bel_pval 80))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval drum1))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname ison) (bel_pval trecerePietoni))
    (ag_bel (bel_type moment) (bel_pobj ?ps3) (bel_pname ison) (bel_pval drum1))


=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Vad trecere de pietonicu pietoni, deci reduc viteza si opresc " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval no)))
    (assert (ag_bel (bel_type moment) (bel_pname isPedestrian) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname isSlowed) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname isIncreased) (bel_pval no)))


    ;(facts AGENT)
)

(defrule AGENT::m3
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname viteza) (bel_pval 0))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval drum1))
    (not (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname ison) (bel_pval trecerePietoni)))

    (ag_bel (bel_type moment) (bel_pobj ?ps3) (bel_pname ison) (bel_pval drum1))


=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Pietonul a trecut, pot creste viteza si porni la drum. " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname isPedestrian) (bel_pval no)))
    (assert (ag_bel (bel_type moment) (bel_pname isSlowed) (bel_pval no)))
    (assert (ag_bel (bel_type moment) (bel_pname isIncreased) (bel_pval yes)))

    ;(facts AGENT)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; DRUM INGUST ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule AGENT::d1
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname direction) (bel_pval ahead))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname direction) (bel_pval ahead))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname nearest) (bel_pval 3))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname nearest) (bel_pval 3))
    (ag_bel (bel_type moment) (bel_pobj ?ps3) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps4) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps5) (bel_pname distanceFromAgent) (bel_pval 3))
    (ag_bel (bel_type moment) (bel_pobj ?ps6) (bel_pname distanceFromAgent) (bel_pval 3))
    (ag_bel (bel_type moment) (bel_pobj ?ps7) (bel_pname dimension) (bel_pval small))
    (ag_bel (bel_type moment) (bel_pobj ?ps8) (bel_pname dimension) (bel_pval big))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Agentul poate intra in locul liber pentru ca are dimensiune mica. " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname park-agent) (bel_pval yes)))

    ;(facts AGENT)
)

(defrule AGENT::d2
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname direction) (bel_pval ahead))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname direction) (bel_pval ahead))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname nearest) (bel_pval 0))
    (ag_bel (bel_type moment) (bel_pobj ?ps3) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps4) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps7) (bel_pname dimension) (bel_pval small))
    (ag_bel (bel_type moment) (bel_pobj ?ps8) (bel_pname dimension) (bel_pval big))
    (ag_bel (bel_type moment) (bel_pobj ?ps8) (bel_pname distanceBetweenCars) (bel_pval 3))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Agentul poate porni, cealalta masina a trecut. " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname isSlowed) (bel_pval no)))
    (assert (ag_bel (bel_type moment) (bel_pname isIncreased) (bel_pval yes)))

    ;(facts AGENT)
)

(defrule AGENT::d3
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps1) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname direction) (bel_pval ahead))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname direction) (bel_pval ahead))
    (ag_bel (bel_type moment) (bel_pobj ?ps3) (bel_pname ison) (bel_pval banda1))
    (ag_bel (bel_type moment) (bel_pobj ?ps4) (bel_pname ison) (bel_pval banda2))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname nearest) (bel_pval ?distanceAgent))

    (test (>= ?distanceAgent 3))    
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>Agentul incetineste pentru a ajunge la locul lui." crlf))
    (assert (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval no)))
    (assert (ag_bel (bel_type moment) (bel_pname isSlowed) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname isIncreased) (bel_pval no)))

    ;(facts AGENT)
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TRAMVAI ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule AGENT::rdi
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname isa) (bel_pval car))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname type) (bel_pval tramvai))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname distanta_de_trotuar) (bel_pval suficienta))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname isa) (bel_pval drum_sens_dublu))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>rdi Vad tramvai, sunt pe un drum cu 2 sensuri si am loc sa il depasesc pe dreapta. " depasire_interzisa crlf))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-right) (bel_pval yes)))
    ;(facts AGENT)
)

(defrule AGENT::rdi2
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname isa) (bel_pval car))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname type) (bel_pval tramvai))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname isa) (bel_pval drum_sens_unic))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname distanta_de_trotuar_dreapta) (bel_pval suficienta))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname distanta_de_trotuar_stanga) (bel_pval suficienta))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>rdi2 Vad tramvai, sunt pe un drum cu sens unic si am loc sa il depasesc pe ambele parti. " depasire_interzisa crlf))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-right) (bel_pval yes)))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-left) (bel_pval yes)))
    ;(facts AGENT)
)

(defrule AGENT::rdi4
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname isa) (bel_pval car))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname type) (bel_pval tramvai))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname isa) (bel_pval drum_sens_unic))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname distanta_de_trotuar_dreapta) (bel_pval insuficienta))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname distanta_de_trotuar_stanga) (bel_pval suficienta))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>rdi4 Vad tramvai, sunt pe un drum cu sens unic si am loc sa il depasesc pe stanga.  " depasire_interzisa crlf))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-left) (bel_pval yes)))
    ;(facts AGENT)
)

(defrule AGENT::rdi5
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname isa) (bel_pval car))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname type) (bel_pval tramvai))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname isa) (bel_pval drum_sens_unic))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname distanta_de_trotuar_dreapta) (bel_pval suficienta))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname distanta_de_trotuar_stanga) (bel_pval insuficienta))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>rdi5 Vad tramvai, sunt pe un drum cu sens unic si am loc sa il depasesc pe dreapta. " depasire_interzisa crlf))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-right) (bel_pval yes)))
    ;(facts AGENT)
)

(defrule AGENT::rdi3
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname isa) (bel_pval car))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname type) (bel_pval tramvai))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname distanta_de_trotuar) (bel_pval insuficienta))
    (ag_bel (bel_type moment) (bel_pobj ?ps2) (bel_pname isa) (bel_pval drum_sens_dublu))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>rdi3 Vad tramvai, sunt pe un drum cu 2 sensuri si nu am loc sa il depasesc pe dreapta, deci il depasesc pe stanga.  " depasire_interzisa crlf))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-left) (bel_pval yes)))
    ;(facts AGENT)
)

;-------------------------------------------------------------------------------------------------------
;
;               Validam ce manevre putem efectua pentru a depasi Tramvaiul
;
;-------------------------------------------------------------------------------------------------------

(defrule AGENT::validate-overtaking-both-sides
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname overtaking-right-maneuver) (bel_pval prohibited))
    ?g <- (ag_bel (bel_type moment) (bel_pname overtaking-left-maneuver) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname overtaking-right) (bel_pval yes))
    (ag_bel (bel_type moment) (bel_pname overtaking-left) (bel_pval yes))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-overtaking-both-sides" crlf))
    (retract ?f)
    (retract ?g)
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-right-maneuver) (bel_pval allowed)))
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-left-maneuver) (bel_pval allowed)))
    ;(facts AGENT)
)


(defrule AGENT::validate-overtaking-left
    (declare (salience -10))
    ?g <- (ag_bel (bel_type moment) (bel_pname overtaking-left-maneuver) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname overtaking-left) (bel_pval yes))
    (not (ag_bel (bel_type moment) (bel_pname overtaking-right) (bel_pval yes)))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-overtaking-left" crlf))
    (retract ?g)
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-left-maneuver) (bel_pval allowed)))
    ;(facts AGENT)
)

(defrule AGENT::validate-overtaking-right
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname overtaking-right-maneuver) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname overtaking-right) (bel_pval yes))
    (not (ag_bel (bel_type moment) (bel_pname overtaking-left) (bel_pval yes)))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-overtaking-right" crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-right-maneuver) (bel_pval allowed)))
    ;(facts AGENT)
)

;------------------------------------------------------------------------------------------------------------------------------------------------------


;-----------------------------------------------------------------------
;
;       Validam ce manevre putem efectua pentru trecerea de pietoni
;
;-----------------------------------------------------------------------

(defrule AGENT::validate-move-car
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname move-car) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes))
    (ag_bel (bel_type moment) (bel_pname isPedestrian) (bel_pval no))
    (ag_bel (bel_type moment) (bel_pname isIncreased) (bel_pval yes))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-move-car" crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname move-car) (bel_pval allowed)))
    ;(facts AGENT)
)

(defrule AGENT::validate-increase-velo
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname increase-velo) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes))
    (ag_bel (bel_type moment) (bel_pname isIncreased) (bel_pval yes))
    (ag_bel (bel_type moment) (bel_pname isSlowed) (bel_pval no))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-increase-velo" crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname increase-velo) (bel_pval allowed)))
    ;(facts AGENT)
)

(defrule AGENT::validate-decrease-velo
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname decrease-velo) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval no))
    (ag_bel (bel_type moment) (bel_pname isSlowed) (bel_pval yes))
    (ag_bel (bel_type moment) (bel_pname isIncreased) (bel_pval no))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-decrease-velo" crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname decrease-velo) (bel_pval allowed)))
    ;(facts AGENT)
)

;-----------------------------------------------------------------------
;
;       Validam ce manevre putem efectua pentru a permite circulatia pe drum ingust
;
;-----------------------------------------------------------------------

(defrule AGENT::validate-parkEmptyPlace
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname park-empty-place) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname park-agent) (bel_pval yes))
    (ag_bel (bel_type moment) (bel_pname isSlowed) (bel_pval yes))


=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-parkEmptyPlace" crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname park-empty-place) (bel_pval allowed)))
    ;(facts AGENT)
)

;-----------------------------------------------------------------------
;
;       Validam ce manevre putem efectua pentru scenariu cu bus
;
;-----------------------------------------------------------------------

(defrule AGENT::validate-overtakingWhenBus
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname overtaking-when-bus) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes))
    (ag_bel (bel_type moment) (bel_pname overtake) (bel_pval yes))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-overtakingWhenBus" crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname overtaking-when-bus) (bel_pval allowed)))
    ;(facts AGENT)
)

(defrule AGENT::validate-waitForCarToPass
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname wait-for-car) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname free-lane) (bel_pval no))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-waitForCarToPass" crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname wait-for-car) (bel_pval allowed)))
    ;(facts AGENT)
)

;-----------------------------------------------------------------------
;
;       Validam ce manevre putem efectua scenariu intersectie
;
;-----------------------------------------------------------------------

(defrule AGENT::validate-waitForCarToPass
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname let-car-move-left) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname allow-car-move-left) (bel_pval yes))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-waitForCarToPass" crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname let-car-move-left) (bel_pval allowed)))
    ;(facts AGENT)
)

(defrule AGENT::validate-leaveIntersection
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname move-car) (bel_pval prohibited))
    (ag_bel (bel_type moment) (bel_pname can-move-car) (bel_pval yes))

=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-leaveIntersection" crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname move-car) (bel_pval allowed)))
    ;(facts AGENT)
)

;-----------------------------------------------------------------------
;
;       Scenariu Intersectie nesemnalizata(prioritate de dreapta)
;
;-----------------------------------------------------------------------

;-----------------------------------------------------------------------
;
;       Initializam manevra
;
;-----------------------------------------------------------------------

(defrule AGENT::initCycle-move
    (declare (salience 89))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-move move by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname move-maneuver) (bel_pval move))) 
    ;(facts AGENT)
)


(defrule AGENT::intersectie1
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname isa) (bel_pval car))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname type) (bel_pval simple_car))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval drum_prioritar))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname proximity) (bel_pval ?distanta))
    (test (<= ?distanta 5))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>rdi Daca vine o masina de pe drumul prioritar si se afla la o distanta mai mica sau egala cu 5, atunci ne oprim pentru a minimiza riscul de accident." crlf))
    (assert (ag_bel (bel_type moment) (bel_pname stop_the_car) (bel_pval yes)))
    ;(facts AGENT)
)

(defrule AGENT::intersectie2
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname isa) (bel_pval trecere_pietoni))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname is) (bel_pval busy))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname proximity) (bel_pval 0))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>rdi Daca trecerea de pietoni imediata este ocupata, ne oprim. " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname stop_the_car) (bel_pval yes)))
    ;(facts AGENT)
)

(defrule AGENT::intersectie3
    (timp (valoare ?t))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname isa) (bel_pval trecere_pietoni))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname is) (bel_pval busy))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname proximity) (bel_pval ?distanta))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname isa) (bel_pval car))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname type) (bel_pval simple_car))
    (ag_bel (bel_type moment) (bel_pobj ?ps) (bel_pname ison) (bel_pval drum_prioritar))
     (test (> ?distanta 1))
=>
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>rdi Daca trecerea de pietoni care este ocupata se afla la o distanta mai mare de 1 si vine o masina de pe un drum prioritar, ne oprim pentru a nu bloca intersectia" crlf))
    (assert (ag_bel (bel_type moment) (bel_pname stop_the_car) (bel_pval yes)))
    ;(facts AGENT)
)

(defrule AGENT::validate-move
    (declare (salience -10))
    ?g <- (ag_bel (bel_type moment) (bel_pname move-maneuver) (bel_pval move))
    (ag_bel (bel_type moment) (bel_pname stop_the_car) (bel_pval yes))
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-move" crlf))
    (retract ?g)
    (assert (ag_bel (bel_type moment) (bel_pname move-maneuver) (bel_pval stop)))
    ;(facts AGENT)
)