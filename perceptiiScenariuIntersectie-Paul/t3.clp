(ag_percept
	(percept_pobj ev1)
	(percept_pname isa)
	(percept_pval eveniment)
)

(ag_percept
	(percept_pobj drum1)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj trecere1)
	(percept_pname isa)
	(percept_pval trecere_pietoni)
)

(ag_percept
	(percept_pobj trecere1)
	(percept_pname isa)
	(percept_pval free)
)

(ag_percept
	(percept_pobj trecere1)
	(percept_pname proximity)
	(percept_pval 0)
)

(ag_percept
	(percept_pobj trecere2)
	(percept_pname isa)
	(percept_pval trecere_pietoni)
)

(ag_percept
	(percept_pobj trecere2)
	(percept_pname is)
	(percept_pval free)
)

(ag_percept
	(percept_pobj trecere2)
	(percept_pname proximity)
	(percept_pval 2)
)



