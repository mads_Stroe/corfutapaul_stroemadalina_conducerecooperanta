(ag_percept
	(percept_pobj ev1)
	(percept_pname isa)
	(percept_pval eveniment)
)

(ag_percept
	(percept_pobj drum1)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj drum1)
	(percept_pname isa)
	(percept_pval drum_sens_unic)
)

(ag_percept
	(percept_pobj car1)
	(percept_pname isa)
	(percept_pval car)
)

(ag_percept
	(percept_pobj car1)
	(percept_pname type)
	(percept_pval tramvai)
)

(ag_percept
	(percept_pobj car1)
	(percept_pname distanta_de_trotuar_dreapta)
	(percept_pval insuficienta)
)

(ag_percept
	(percept_pobj car1)
	(percept_pname distanta_de_trotuar_stanga)
	(percept_pval insuficienta)
)

