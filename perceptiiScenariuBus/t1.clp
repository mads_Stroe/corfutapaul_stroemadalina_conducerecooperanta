; agentul (masina1) este in spatele camionului pe banda2.
; autobuzul este in mers pe banda 1
; depasirea este interzisa

; EVENIMENT
(ag_percept
	(percept_pobj ev1)
	(percept_pname isa)
	(percept_pval eveniment)
)

; MASINI

(ag_percept
	(percept_pobj masina1)
	(percept_pname isa)
	(percept_pval masina)
)

(ag_percept
	(percept_pobj masina1)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj camion1)
	(percept_pname isa)
	(percept_pval camion)
)

(ag_percept
	(percept_pobj camion1)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj bus1)
	(percept_pname isa)
	(percept_pval bus)
)

(ag_percept
	(percept_pobj bus1)
	(percept_pname partof)
	(percept_pval ev1)
)

; BENZI SI MASINI

(ag_percept
	(percept_pobj banda1)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj banda1)
	(percept_pname isa)
	(percept_pval banda)
)

(ag_percept
	(percept_pobj masina1)
	(percept_pname ison)
	(percept_pval banda2)
)

(ag_percept
	(percept_pobj camion1)
	(percept_pname ison)
	(percept_pval banda2)
)

(ag_percept
	(percept_pobj camion1)
	(percept_pname inFrontOf)
	(percept_pval masina1)
)

(ag_percept
	(percept_pobj banda2)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj banda2)
	(percept_pname isa)
	(percept_pval banda)
)

(ag_percept
	(percept_pobj locBus)
	(percept_pname isa)
	(percept_pval loc)
)

(ag_percept
	(percept_pobj locBus)
	(percept_pname ison)
	(percept_pval banda1)
)

