; trecere de pietoni, cu pietoni

(ag_percept
	(percept_pobj ev1)
	(percept_pname isa)
	(percept_pval eveniment)
)

(ag_percept
	(percept_pobj drum1)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj trecerePietoni)
	(percept_pname partof)
	(percept_pval drum1)
)

(ag_percept
	(percept_pobj trecerePietoni)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj masina1)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj masina1)
	(percept_pname ison)
	(percept_pval drum1)
)

(ag_percept
	(percept_pobj masina1)
	(percept_pname viteza)
	(percept_pval 80)
)

(ag_percept
	(percept_pobj drum1)
	(percept_pname tip)
	(percept_pval localitate)
)

(ag_percept
	(percept_pobj pieton1)
	(percept_pname ison)
	(percept_pval trecerePietoni)
)
