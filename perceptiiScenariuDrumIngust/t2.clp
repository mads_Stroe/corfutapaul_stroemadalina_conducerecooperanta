; masinile se apropie si se ia in calcul dimensiunea

; EVENIMENT
(ag_percept
	(percept_pobj ev1)
	(percept_pname isa)
	(percept_pval eveniment)
)

; BANDA
(ag_percept
	(percept_pobj banda1)
	(percept_pname isa)
	(percept_pval banda)
)

(ag_percept
	(percept_pobj banda1)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj banda2)
	(percept_pname isa)
	(percept_pval banda)
)

(ag_percept
	(percept_pobj banda2)
	(percept_pname partof)
	(percept_pval ev1)
)

; MASINA
(ag_percept
	(percept_pobj masina1)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj masina1)
	(percept_pname isa)
	(percept_pval masina)
)

(ag_percept
	(percept_pobj masina1)
	(percept_pname ison)
	(percept_pval banda1)
)

(ag_percept
	(percept_pobj masina1)
	(percept_pname direction)
	(percept_pval ahead)
)

(ag_percept
	(percept_pobj masina1)
	(percept_pname nearest)
	(percept_pval 3)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname isa)
	(percept_pval masina)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname ison)
	(percept_pval banda2)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname direction)
	(percept_pval ahead)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname nearest)
	(percept_pval 3)
)

; LOCURI LIBERE
(ag_percept
	(percept_pobj loc1)
	(percept_pname isa)
	(percept_pval emptySpace)
)

(ag_percept
	(percept_pobj loc1)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj loc1)
	(percept_pname ison)
	(percept_pval banda1)
)

(ag_percept
	(percept_pobj loc1)
	(percept_pname distanceFromAgent)
	(percept_pval 8)
)

(ag_percept
	(percept_pobj loc2)
	(percept_pname isa)
	(percept_pval emptySpace)
)

(ag_percept
	(percept_pobj loc2)
	(percept_pname partof)
	(percept_pval ev1)
)

(ag_percept
	(percept_pobj loc2)
	(percept_pname ison)
	(percept_pval banda2)
)

(ag_percept
	(percept_pobj loc2)
	(percept_pname distanceFromAgent)
	(percept_pval 9)
)

(ag_percept
	(percept_pobj masina1)
	(percept_pname dimension)
	(percept_pval small)
)

(ag_percept
	(percept_pobj masina2)
	(percept_pname dimension)
	(percept_pval big)
)